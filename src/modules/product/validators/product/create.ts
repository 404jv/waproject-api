import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsOptional, IsString, MaxLength, Min } from 'class-validator';
import { IProduct } from '../../../database/interfaces/product';

export class CreateProductValidator implements IProduct {
  @IsOptional()
  @IsString()
  @ApiProperty({ required: false, type: 'string' })
  public id?: string;

  @IsNotEmpty()
  @IsString()
  @MaxLength(150)
  @ApiProperty({ required: true, type: 'string', maxLength: 150 })
  public description: string;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({ required: true, type: 'numeric', minLength: 0 })
  @Min(0)
  public quantity: number;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({ required: true, type: 'numeric', minLength: 0 })
  @Min(0)
  public price: number;
}
