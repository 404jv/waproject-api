import { PaginationValidator } from 'modules/common/validators/pagination';
import { IsString, IsOptional, IsIn } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ListProductValidator extends PaginationValidator {
  @IsString()
  @IsOptional()
  @IsIn(['description', 'quantity', 'price', 'createdDate', 'updatedDate'])
  @ApiProperty({ required: false, enum: ['description', 'quantity', 'price', 'createdDate', 'updatedDate'] })
  public orderBy: string;
}
