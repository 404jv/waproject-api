import { Module } from '@nestjs/common';
import { ProductController } from './controllers/product';
import { ProductRepository } from './repositories/product';
import { ProductService } from './services/product';

@Module({
  controllers: [ProductController],
  providers: [ProductService, ProductRepository]
})
export class ProductModule {}
