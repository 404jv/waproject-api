import { Injectable } from '@nestjs/common';
import { IPaginationParams } from 'modules/common/interfaces/pagination';
import { IProduct } from 'modules/database/interfaces/product';
import { Product } from 'modules/database/models/product';
import { Transaction } from 'objection';

@Injectable()
export class ProductRepository {
  public async insert(model: IProduct, transaction?: Transaction): Promise<Product> {
    return Product.query(transaction).insertAndFetch(model as any);
  }

  public async list(paginationParams: IPaginationParams, transaction?: Transaction): Promise<Product[]> {
    const { page, pageSize, orderBy, orderDirection, term } = paginationParams;

    let query = Product.query(transaction)
      .select('*')
      .page(page, pageSize);

    if (orderBy) {
      query.orderBy(orderBy, orderDirection);
    }

    if (paginationParams.term) {
      query.where('description', 'ilike', `%${term}%`);
    }

    const products = await query;

    return products.results;
  }
}
