import { UnauthorizedException } from '@nestjs/common';
import { IProduct } from 'modules/database/interfaces/product';
import { enRoles, IUser } from 'modules/database/interfaces/user';

import { ProductRepository } from '../repositories/product';
import { ProductService } from '../services/product';

describe('Product', () => {
  let productRepository: ProductRepository;
  let productService: ProductService;

  const product: IProduct = {
    description: 'description',
    price: 100,
    quantity: 4
  };

  const userAdmin: IUser = {
    firstName: 'firstName',
    lastName: 'lastName',
    email: 'test@email.com',
    roles: [enRoles.admin]
  };

  const user: IUser = {
    firstName: 'firstName',
    lastName: 'lastName',
    email: 'test@email.com',
    roles: [enRoles.user]
  };

  beforeEach(() => {
    productRepository = new ProductRepository();
    productService = new ProductService(productRepository);
  });

  it('should be able to create a product', async () => {
    jest.spyOn(productRepository, 'insert').mockImplementationOnce(product => Promise.resolve({ ...product } as any));

    const result = await productService.create({
      product,
      user: userAdmin
    });

    expect(result).toHaveProperty('id');
    expect(result.description).toEqual('description');
    expect(result.price).toEqual(100);
    expect(result.quantity).toEqual(4);
  });

  it('should not be able to create a product when the user is not an admin or sysAdmin', async () => {
    try {
      await productService.create({
        product,
        user
      });
      fail();
    } catch (error) {
      expect(error).toBeInstanceOf(UnauthorizedException);
      expect(error.message.message).toBe('not-allowed-to-create-product');
    }
  });
});
