import { UnauthorizedException, Injectable } from '@nestjs/common';
import { IProduct } from 'modules/database/interfaces/product';
import { enRoles, IUser } from 'modules/database/interfaces/user';
import uuidV4 from 'uuid/v4';

import { Product } from '../../database/models/product';
import { ProductRepository } from '../repositories/product';
import { ListProductValidator } from '../validators/product/list';

interface IRequest {
  product: IProduct;
  user: IUser;
}

@Injectable()
export class ProductService {
  constructor(private productRepository: ProductRepository) {}

  public async create({ product, user }: IRequest): Promise<Product> {
    const { description, price, quantity } = product;

    const isAdmin = this.isAdmin(user.roles);
    if (!isAdmin) {
      throw new UnauthorizedException('not-allowed-to-create-product');
    }

    const productCreated = await this.productRepository.insert({
      id: uuidV4(),
      description,
      price,
      quantity
    });

    return productCreated;
  }

  public async list({ orderBy, orderDirection, page, pageSize, term }: ListProductValidator): Promise<Product[]> {
    const products = await this.productRepository.list({
      orderBy,
      orderDirection,
      page,
      pageSize,
      term
    });

    return products;
  }

  private isAdmin(roles: string[]) {
    if (roles.includes(enRoles.admin)) return true;

    if (roles.includes(enRoles.sysAdmin)) return true;

    return false;
  }
}
