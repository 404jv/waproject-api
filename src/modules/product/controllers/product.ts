import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { AuthRequired, CurrentUser } from 'modules/common/guards/token';
import { ProductService } from '../services/product';
import { CreateProductValidator } from '../validators/product/create';
import { Product } from '../../database/models/product';
import { ListProductValidator } from '../validators/product/list';

@ApiTags('Product')
@Controller()
export class ProductController {
  constructor(private productService: ProductService) {}

  @Post('create')
  @AuthRequired()
  @ApiResponse({ status: 201, type: Product })
  // @ts-ignore
  private async create(@Body() body: CreateProductValidator, @CurrentUser() user: ICurrentUser) {
    const product = await this.productService.create({
      product: body,
      user
    });

    return product;
  }

  @Get('list')
  @ApiResponse({ status: 200, type: Product })
  // @ts-ignore
  private async list(@Query() { orderBy, orderDirection, page, pageSize, term }: ListProductValidator) {
    const products = await this.productService.list({
      orderBy,
      orderDirection,
      page,
      pageSize,
      term
    });

    return products;
  }
}
