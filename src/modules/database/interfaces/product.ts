export interface IProduct {
  id?: string;
  description: string;
  quantity: number;
  price: number;
}
