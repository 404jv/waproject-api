import * as Knex from 'knex';

export async function up(knex: Knex): Promise<any> {
  await knex.schema.createTable('Product', table => {
    table
      .string('id', 150)
      .notNullable()
      .primary();

    table.string('description', 150).notNullable();
    table.integer('quantity').notNullable();
    table.decimal('price').notNullable();
    table.dateTime('createdDate').notNullable();
    table.dateTime('updatedDate').notNullable();
  });
}

export async function down(knex: Knex): Promise<any> {
  await knex.schema.dropTable('Product');
}
